FROM docker:stable

ARG DEVO_VERSION=0.8.5

ENV PYTHONUNBUFFERED 1

RUN apk --no-cache add curl python3 git jq

RUN curl -Lo /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v1.14.4/bin/linux/amd64/kubectl \
 && chmod +x /usr/local/bin/kubectl

RUN curl -Lo /usr/local/bin/skaffold https://storage.googleapis.com/skaffold/releases/v0.34.0/skaffold-linux-amd64 \
 && chmod +x /usr/local/bin/skaffold

RUN curl -Lo /usr/local/bin/kustomize https://github.com/kubernetes-sigs/kustomize/releases/download/v3.0.3/kustomize_3.0.3_linux_amd64 \
 && chmod +x /usr/local/bin/kustomize

RUN pip3 install --no-cache-dir --upgrade devo==$DEVO_VERSION
